var utente = idUtente;

function salvaAttivita() {
	console.log('Login');

	var attivitaData = {
		nome: $('#nomeAttivitaForm').val(),
		data: new Date ($('#dataAttivitaForm').val()),
		centro : idCentro
	}

	if (!attivitaData.nome || !attivitaData.data) {
		var message = '<p>Tutti icampi sono obbligatori !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(2500).fadeOut();
		return;
	}
	if (!idUtente) {
		var message = '<p>Errore nel salvataggio, è necessario loggarsi !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(3500).fadeOut();
		return;
	}
	if (!idCentro) {
		var message = '<p>Errore nel salvataggio, è necessario essere un centro !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(3500).fadeOut();
		return;
	}

	$.ajax({
		data: JSON.stringify(attivitaData),
		type: 'post',
		contentType: 'application/json;charset=UTF-8',
		url: 'attivita'
	}).done(function (data) {
		var message = '';
		if(!data){
			message = '<p>Salvataggio riuscito !</p>';
		} else {
			message = 'Errore nel salvataggio!';
		}
		$('#message').empty().append(message).fadeIn(0).delay(2500).fadeOut();
	});
}
