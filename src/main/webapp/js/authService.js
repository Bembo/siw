var idUtente = undefined;
var tipoUtente = undefined;
var idCentro = undefined;

function login() {
	console.log('Login');

	var userData = {
		username: $('#username').val(),
		password: $('#password').val() 
	}

	if (!userData.username || !userData.password) {
		var message = '<p>Username e Password sono campi obbligatori !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(2500).fadeOut();
		return;
	}

	$.ajax({
		data: JSON.stringify(userData),
		type: 'post',
		contentType: 'application/json;charset=UTF-8',
		url: 'auth'
	}).done(function (data) {
		if (data.errorMessage !== null && data.errorMessage !== undefined) {		
			var message = '<p>Errore nel login !</p>';
			$('#message').empty().append(message).fadeIn(0).delay(2500).fadeOut();
		} else {
			idUtente = data.idUtente;
			tipoUtente = data.tipoUtente;
			$('#login').hide();
			if(data.tipoUtente === 1){					// se utente amministrazione
				$('#enableAzienda').show();
			} else if(data.tipoUtente === 2) {			// se utente centro
				idCentro = data.idCentro;
				$('#enableCentro').show();
			} else if(data.tipoUtente === 3) {			// se utente allievo
				initListAllievo();
				$('#enabledAllievi').show();
			}
			
		}
	});
}
