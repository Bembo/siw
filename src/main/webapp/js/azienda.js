
function salvaCentro() {
	console.log('Login');
	
	var centroData = {
		nome: $('#nomeCentroForm').val(),
		indirizzo: $('#indirizzoCentroForm').val(),
		telefono: $('#telefonoCentroForm').val(),
		capienza: $('#capienzaCentroForm').val(),
		email: $('#emailCentroForm').val()
	}

	if (!centroData.nome || !centroData.indirizzo || !centroData.telefono || !centroData.capienza || !centroData.email) {
		var message = '<p>Tutti i campi sono obbligatori !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(3500).fadeOut();
		return;
	}
	
	if (!idUtente) {
		var message = '<p>Errore nel salvataggio, è necessario loggarsi !</p>';
		$('#message').empty().append(message).fadeIn(0).delay(3500).fadeOut();
		return;
	}

	$.ajax({
		data: JSON.stringify(centroData),
		type: 'post',
		contentType: 'application/json;charset=UTF-8',
		url: 'centri'
	}).done(function (data) {
		var message = '';
		if(data!=null){
			message = '<p>Salvataggio riuscito !</p>';
		} else {
			message = 'Errore nel salvataggio!';
		}
		$('#message').empty().append(message).fadeIn(0).delay(3500).fadeOut();
	});
}
