function initListAllievo() {
	$.ajax({
		data: JSON.stringify(centroData),
		type: 'get',
		contentType: 'application/json;charset=UTF-8',
		url: 'attivita/' + idUtente
	}).done(function (data) {
		var message = '';
		if(data!=null){
			for (var i = 0; i < data.length; i++) {
				$('#listAttivita').append('<li class="list-group-item">' + data[i] + '</li>');
			}
		} else {
			message = 'Errore nel recuper dei dati!';
		}
	});
}