package it.uniromatre.siw.siw2018.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.uniromatre.siw.siw2018.dto.AttivitaDto;
import it.uniromatre.siw.siw2018.service.AttivitaService;

@RestController
@RequestMapping("/attivita")
public class AttivitaController {

	@Autowired
	private AttivitaService attivitaService;
	
	@RequestMapping(method = RequestMethod.POST)
	public AttivitaDto salvaAttivita(@RequestBody AttivitaDto dto) {
		return this.attivitaService.salvaAttivita(dto);
	}
	
	@RequestMapping(value = "/{idAttivita}/{idAllievo}", method = RequestMethod.PUT)
	public String aggiungiAllievo(@PathVariable("idAttivita") Long idAttivita, @PathVariable("idAllievo") Long idAllievo) {
		try {
			this.attivitaService.aggiungiAllievo(idAttivita,idAllievo);
			return "ok";
		} catch(Exception exe) {
			return "Errore nel salvataggio";
		}
	}

	@RequestMapping(value = "/{idAllievo}", method = RequestMethod.GET)
	public List<String> getAttivitaByAllievo(@PathVariable("idAllievo") Long idAllievo) {
		return this.attivitaService.attivitaByAllievo(idAllievo);
	}
	
}
