package it.uniromatre.siw.siw2018.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.uniromatre.siw.siw2018.bean.LoginRequest;
import it.uniromatre.siw.siw2018.bean.LoginResponse;
import it.uniromatre.siw.siw2018.model.Utente;
import it.uniromatre.siw.siw2018.service.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthService authService;
	
	@RequestMapping(method = RequestMethod.POST)
	public LoginResponse login(@RequestBody LoginRequest loginRequest) {
		List<Utente> utenti = this.authService.login(loginRequest.getUsername(), loginRequest.getPassword());
		LoginResponse response = new LoginResponse();
		if(utenti.size() == 0 ) {
			response.setErrorMessage("nessun utente");
		}else {
			Utente loggato = utenti.get(0);
			response.setIdUtente(loggato.getId());
			response.setTipoUtente(loggato.getTipologiaUtente());
			response.setIdCentro(loggato.getIdCentro());
		}
		
		return response;
		
	}
}
