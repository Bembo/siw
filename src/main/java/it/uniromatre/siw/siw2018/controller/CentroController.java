package it.uniromatre.siw.siw2018.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.uniromatre.siw.siw2018.dto.CentroDto;
import it.uniromatre.siw.siw2018.service.CentroService;

@RestController
@RequestMapping("/centri")
public class CentroController {

	@Autowired
	private CentroService centroService;
	
	@RequestMapping(method = RequestMethod.POST)
	public CentroDto salvaCentro(@RequestBody CentroDto dto) {
		return this.centroService.salvaCentro(dto);
	}
	

}
