package it.uniromatre.siw.siw2018.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.service.AllievoService;

@RestController
@RequestMapping("/allievi")
public class AllievoController {

	@Autowired
	private AllievoService allievoService;
	
	@RequestMapping(method = RequestMethod.POST)
	public AllievoDto salvaAllievo(@RequestBody AllievoDto allievoDto) {
		return this.allievoService.salvaAllievo(allievoDto);
	}
		
}
