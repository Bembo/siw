package it.uniromatre.siw.siw2018;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Siw2018Application {

	public static void main(String[] args) {
		SpringApplication.run(Siw2018Application.class, args);
	}
}
