package it.uniromatre.siw.siw2018.mapper;

import java.util.ArrayList;
import java.util.List;

import it.uniromatre.siw.siw2018.dto.AttivitaDto;
import it.uniromatre.siw.siw2018.model.Attivita;
import it.uniromatre.siw.siw2018.model.Centro;



public class AttivitaMapper {
	

	public static AttivitaDto toDto(Attivita entity) {
		AttivitaDto dto = new AttivitaDto();
		dto.setNome(entity.getNome());
		dto.setData(entity.getData());
		dto.setCentro(entity.getCentro().getId());
		return dto;
	}
	
	public static Attivita toEntity(AttivitaDto dto, Centro centro) {
		Attivita attivita = new Attivita();
		attivita.setNome(dto.getNome());
		attivita.setData(dto.getData());
		attivita.setCentro(centro);
		return attivita;
	}
	
	public static List<AttivitaDto> toDtos(List<Attivita> entities) {
		List<AttivitaDto> dtos = new ArrayList<>();
		entities.stream().forEach(e -> dtos.add(toDto(e)));
		return dtos;
	}
	
}
