package it.uniromatre.siw.siw2018.mapper;

import java.util.ArrayList;
import java.util.List;

import it.uniromatre.siw.siw2018.dto.CentroDto;
import it.uniromatre.siw.siw2018.model.Centro;

public class CentroMapper {

	public static CentroDto toDto(Centro entity) {
		CentroDto dto = new CentroDto();
		
		dto.setTelefono(entity.getTelefono());
		dto.setNome(entity.getNome());
		dto.setIndirizzo(entity.getIndirizzo());
		dto.setEmail(entity.getEmail());
		dto.setCapienza(entity.getCapienza());
		
		return dto;
	}
	
	public static Centro toEntity(CentroDto dto) {
		Centro centro = new Centro();
		
		centro.setTelefono(dto.getTelefono());
		centro.setCapienza(dto.getCapienza());
		centro.setEmail(dto.getEmail());
		centro.setNome(dto.getNome());
		centro.setIndirizzo(dto.getIndirizzo());
		
		return centro;
	}
	
	public static List<CentroDto> toDtos(List<Centro> entities) {
		List<CentroDto> dtos = new ArrayList<>();
		entities.stream().forEach(e -> dtos.add(toDto(e)));
		return dtos;
	}
	
}
