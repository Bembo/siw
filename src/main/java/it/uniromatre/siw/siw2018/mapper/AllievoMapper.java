package it.uniromatre.siw.siw2018.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.model.Allievo;
import it.uniromatre.siw.siw2018.model.Attivita;

public class AllievoMapper {

	/**
	 * From entity to dto
	 * 
	 * @param entity
	 * @return dto
	 */
	public static AllievoDto toDto(Allievo entity) {
		AllievoDto dto = new AllievoDto();
		dto.setTelefono(entity.getTelefono());
		dto.setCognome(entity.getCognome());
		dto.setDataNascita(entity.getDataNascita());
		dto.setEmail(entity.getEmail());
		dto.setLuogoNascita(entity.getLuogoNascita());
		dto.setNome(entity.getNome());
		dto.setAttivita(setAttivita(entity.getAttivita()));
		
		return dto;
	}
	
	/**
	 * From list entity to list dto
	 * 
	 * @param entities
	 * @return
	 */
	public static List<AllievoDto> toDtos(List<Allievo> entities) {
		List<AllievoDto> dtos = new ArrayList<>();
		entities.stream().forEach(e -> dtos.add(toDto(e)));
		return dtos;
	}
	
	/**
	 * From dto to entity
	 * 
	 * @param centri
	 * @return
	 */
	public static Allievo toEntity(AllievoDto dto) {
		Allievo allievo = new Allievo();
		
		allievo.setTelefono(dto.getTelefono());
		allievo.setCognome(dto.getCognome());
		allievo.setDataNascita(dto.getDataNascita());
		allievo.setEmail(dto.getEmail());
		allievo.setLuogoNascita(dto.getLuogoNascita());
		allievo.setNome(dto.getNome());
		allievo.setAttivita(new HashSet<>());
		
		return allievo;
	}
	
	private static Set<Long> setAttivita(Set<Attivita> attivita) {
		Set<Long> attivitaId = new HashSet<>();
		attivita.stream().forEach(c -> attivitaId.add(c.getId()));
		return attivitaId;
	}


	
}
