package it.uniromatre.siw.siw2018.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniromatre.siw.siw2018.model.Utente;
import it.uniromatre.siw.siw2018.repository.UtenteRepository;

@Service
public class AuthServiceImpl implements AuthService{

	@Autowired
	private UtenteRepository utenteRepository;
	
	@Override
	public List<Utente> login(String username, String password) {
		return this.utenteRepository.findByUsernameAndPassword(username, password);
	}

}
