package it.uniromatre.siw.siw2018.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.mapper.AllievoMapper;
import it.uniromatre.siw.siw2018.model.Allievo;
import it.uniromatre.siw.siw2018.repository.AllievoRepository;

@Service
public class AllievoServiceImpl implements AllievoService {

	@Autowired
	private AllievoRepository allievoRepository;
	
	@Override
	public AllievoDto salvaAllievo(AllievoDto allievo) {
		return AllievoMapper.toDto(this.allievoRepository.save(AllievoMapper.toEntity(allievo)));
	}

	@Override
	public AllievoDto findByEmail(String email) {
		return AllievoMapper.toDto(this.allievoRepository.findByEmail(email));
		//return this.allievoRepository.findByEmail(email);   prima ritornava un Allievo
	}

	@Override
	public List<AllievoDto> findAll() {
		return AllievoMapper.toDtos(this.allievoRepository.findAll());
		//return (List<Allievo>) this.allievoRepository.findAll();
	}
	
	@Override
	public AllievoDto findById(Long id) {
		//Optional<AllievoDto> customer = AllievoMapper.toDtoOptional(this.allievoRepository.findById(id));
		Optional<Allievo> customer = this.allievoRepository.findById(id);
		if (customer.isPresent()) 
			return AllievoMapper.toDto(customer.get()); //prima il ritornava solo customer.get()
		else
			return null;
	}

	@Override
	public boolean emailAlreadyExists(AllievoDto customer) {
		Allievo check = this.allievoRepository.findByEmail(customer.getEmail());		//sarebbe meglio creare un AllievoDto check?
		if (check!=null) {
			return true;
		}
		else 
			return false;
	}
	
	@Override
	public void deleteByEmail(String email) {
		this.allievoRepository.deleteByEmail(email);
	}
}
