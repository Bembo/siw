package it.uniromatre.siw.siw2018.service;

import java.util.List;

import it.uniromatre.siw.siw2018.dto.AllievoDto;

public interface AllievoService {

	public AllievoDto salvaAllievo(AllievoDto allievo);

	void deleteByEmail(String email);

	//prima il parametro era di tipo Allievo
	boolean emailAlreadyExists(AllievoDto customer);

	//prima il tipo era Allievo e non AllievoDto
	AllievoDto findByEmail(String email);

	//prima la lista era di tipo Allievo
	List<AllievoDto> findAll();

	AllievoDto findById(Long id);

	
	
}
