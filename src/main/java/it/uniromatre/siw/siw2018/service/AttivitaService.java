package it.uniromatre.siw.siw2018.service;

import java.util.List;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.dto.AttivitaDto;

public interface AttivitaService {

	public AttivitaDto salvaAttivita(AttivitaDto dto);
	
	public void aggiungiAllievo(Long idAttivita, Long idAllievo);
	
	public void aggiungiAllievi(List<Long> idAllievi, Long idAttivita);

	boolean alreadyExists(AttivitaDto attivita);

	List<AttivitaDto> findByCentro(Long idCentro);

	void deleteByNome(String name);

	AttivitaDto findByNome(String nome);

	List<AttivitaDto> findAll();

	AttivitaDto findById(Long id);

	boolean alreadySigned(AttivitaDto attivita, AllievoDto allievo);
	
	public List<String> attivitaByAllievo(Long idAllievo);
	
}
