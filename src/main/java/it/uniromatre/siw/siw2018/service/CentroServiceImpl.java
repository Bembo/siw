package it.uniromatre.siw.siw2018.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniromatre.siw.siw2018.dto.CentroDto;
import it.uniromatre.siw.siw2018.mapper.CentroMapper;
import it.uniromatre.siw.siw2018.model.Centro;
import it.uniromatre.siw.siw2018.repository.CentroRepository;

@Service
public class CentroServiceImpl implements CentroService{

	@Autowired
	private CentroRepository centroRepository;
	
	@Override
	public CentroDto salvaCentro(CentroDto dto) {
		return CentroMapper.toDto(this.centroRepository.save(CentroMapper.toEntity(dto)));
	}
	
	@Override
	public List<CentroDto> findAll() {
		return (List<CentroDto>) CentroMapper.toDtos(this.centroRepository.findAll());
	}

	@Override
	public CentroDto findById(Long id) {
		Optional<Centro> centro = this.centroRepository.findById(id);
		if(centro.isPresent())
			return CentroMapper.toDto(centro.get());
		else 
			return null;
	}
	
	@Override
	public boolean nomeAlreadyExists(CentroDto centro) {
		CentroDto check = CentroMapper.toDto(this.centroRepository.findByNome(centro.getNome()));
		if (check!=null) {
			return true;
		}
		else 
			return false;
	}

}
