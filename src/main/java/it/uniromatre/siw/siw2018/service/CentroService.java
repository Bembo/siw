package it.uniromatre.siw.siw2018.service;

import java.util.List;

import it.uniromatre.siw.siw2018.dto.CentroDto;

public interface CentroService {

	public CentroDto salvaCentro(CentroDto dto);
	
	public List<CentroDto> findAll();
	public CentroDto findById(Long id);
	public boolean nomeAlreadyExists(CentroDto centro);


	
}
