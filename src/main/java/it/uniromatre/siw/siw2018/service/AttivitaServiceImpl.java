package it.uniromatre.siw.siw2018.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.dto.AttivitaDto;
import it.uniromatre.siw.siw2018.mapper.AllievoMapper;
import it.uniromatre.siw.siw2018.mapper.AttivitaMapper;
import it.uniromatre.siw.siw2018.model.Allievo;
import it.uniromatre.siw.siw2018.model.Attivita;
import it.uniromatre.siw.siw2018.model.Centro;
import it.uniromatre.siw.siw2018.repository.AllievoRepository;
import it.uniromatre.siw.siw2018.repository.AttivitaRepository;
import it.uniromatre.siw.siw2018.repository.CentroRepository;

@Service
public class AttivitaServiceImpl implements AttivitaService {

	@Autowired
	private AttivitaRepository attivitaRepository;
	
	@Autowired
	private CentroRepository centroRepository;
	
	@Autowired
	private AllievoRepository allievoRepository;
	
	public List<String> attivitaByAllievo(Long idAllievo) {
		List<Attivita> allAttivita = this.attivitaRepository.findAll();
		List<String> attivitaAllievo = new ArrayList<>();
		Optional<Allievo> allievo =  this.allievoRepository.findById(idAllievo);
		for (Attivita attivita : allAttivita) {
			if(null != attivita.getAllievi() && attivita.getAllievi().contains(allievo.get()) ) {
				attivitaAllievo.add(attivita.getNome() + " - " + attivita.getData().toString());
			}
		}
		return attivitaAllievo;
	}
	
	@Override
	public AttivitaDto salvaAttivita(AttivitaDto dto) {
		Optional<Centro> centro = this.centroRepository.findById(dto.getCentro());
		return AttivitaMapper.toDto(this.attivitaRepository.save(AttivitaMapper.toEntity(dto, centro.get())));
	}

	@Override
	public void aggiungiAllievo(Long idAttivita, Long idAllievo) {
		Optional<Allievo> allievo = this.allievoRepository.findById(idAllievo);
		Optional<Attivita> attivita = this.attivitaRepository.findById(idAttivita);
		if(allievo.isPresent() && attivita.isPresent()) {
			attivita.get().addAllievo(allievo.get());
		}
		
	}

	@Override
	public void aggiungiAllievi(List<Long> idAllievi, Long idAttivita) {
		idAllievi.stream().forEach(a -> aggiungiAllievo(a, idAttivita));
	}
	
	@Override
	public boolean alreadyExists(AttivitaDto attivita) {
		List<AttivitaDto> attivitas= AttivitaMapper.toDtos(this.attivitaRepository.findByName(attivita.getNome()));	

		if(attivitas.size()>0) 
			return true;
		else
			return false;
	}
	
	@Override
	public boolean alreadySigned(AttivitaDto attivita, AllievoDto allievo) {
		for (AllievoDto a : AllievoMapper.toDtos(attivita.getAllievi())) {
			if(a.equals(allievo))
				return true; 
		}
		return false; 
	}
	
	@Override
	public List<AttivitaDto> findAll() {
		return (List<AttivitaDto>) AttivitaMapper.toDtos(this.attivitaRepository.findAll());
	}

	@Override
	public AttivitaDto findById(Long id) {
		Optional<Attivita> attivita = this.attivitaRepository.findById(id);
		if(attivita.isPresent())
			return AttivitaMapper.toDto(attivita.get());
		else 
			return null;
	}
	
	@Override
	public AttivitaDto findByNome(String nome) {
		 List<Attivita> attivita = this.attivitaRepository.findByName(nome);
		 if(!attivita.isEmpty())
			 return AttivitaMapper.toDto(attivita.get(0));
		 else 
			 return null;
	}
	
	@Override
	public void deleteByNome(String name) {
		this.attivitaRepository.deleteByName(name);
	}
	
	@Override
	public List<AttivitaDto> findByCentro(Long idCentro){
		Optional<Centro> centro = this.centroRepository.findById(idCentro);
		return AttivitaMapper.toDtos(this.attivitaRepository.findByCentro(centro.get())) ;
	}
}


