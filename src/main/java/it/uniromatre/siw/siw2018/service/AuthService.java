package it.uniromatre.siw.siw2018.service;

import java.util.List;

import it.uniromatre.siw.siw2018.model.Utente;

public interface AuthService {

	public List<Utente> login(String username,String password);
}
