package it.uniromatre.siw.siw2018.bean;

import java.io.Serializable;

public class LoginResponse implements Serializable {

	private static final long serialVersionUID = 4613735191424793670L;

	private Long idCentro;
	private Long idUtente;
	private Long tipoUtente;
	private String errorMessage;
	

	/**
	 * @return the idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente
	 *            the idUtente to set
	 */
	public void setIdUtente(Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * @return the tipoUtente
	 */
	public Long getTipoUtente() {
		return tipoUtente;
	}

	/**
	 * @param tipoUtente
	 *            the tipoUtente to set
	 */
	public void setTipoUtente(Long tipoUtente) {
		this.tipoUtente = tipoUtente;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Long getIdCentro() {
		return idCentro;
	}

	public void setIdCentro(Long idCentro) {
		this.idCentro = idCentro;
	}

}
