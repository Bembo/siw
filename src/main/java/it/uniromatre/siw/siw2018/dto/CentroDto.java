package it.uniromatre.siw.siw2018.dto;

import java.io.Serializable;

public class CentroDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private String indirizzo;
	private String email;
	private String telefono;
	private Integer capienza;

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the indirizzo
	 */
	public String getIndirizzo() {
		return indirizzo;
	}

	/**
	 * @param indirizzo
	 *            the indirizzo to set
	 */
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the capienza
	 */
	public Integer getCapienza() {
		return capienza;
	}

	/**
	 * @param capienza
	 *            the capienza to set
	 */
	public void setCapienza(Integer capienza) {
		this.capienza = capienza;
	}

}
