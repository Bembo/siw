package it.uniromatre.siw.siw2018.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


import it.uniromatre.siw.siw2018.model.Allievo;



public class AttivitaDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private Date data;
    private Long centro;
    private List<Allievo> allievi;
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the data
	 */
	public Date getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Date data) {
		this.data = data;
	}
	/**
	 * @return the centro
	 */
	public Long getCentro() {
		return centro;
	}
	/**
	 * @param centro the centro to set
	 */
	public void setCentro(Long centro) {
		this.centro = centro;
	}
	public List<Allievo> getAllievi() {
		return allievi;
	}
	public void setAllievi(List<Allievo> allievi) {
		this.allievi = allievi;
	}
    
    
}
