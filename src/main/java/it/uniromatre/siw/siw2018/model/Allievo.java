package it.uniromatre.siw.siw2018.model;

/**
 * Alliveo entity
 */
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ALLIEVO")
public class Allievo {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NAME", length = 1024)
	private String name;

	@Column(name = "COGNOME", length = 1024)
	private String surname;

	@Column(name = "LUOGONASCITA", length = 1024)
	private String birthplace;

	@Column(name = "EMAIL", length = 1024)
	private String email;

	@Column(name = "TELEFONO", length = 1024)
	private String numberphone;

	@Column(name = "DATANASCITA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataNascita;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "allievi")
	private Set<Attivita> attivita = new HashSet<>();

	public Allievo() {}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return name;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.name = nome;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return surname;
	}

	/**
	 * @param cognome
	 *            the cognome to set
	 */
	public void setCognome(String cognome) {
		this.surname = cognome;
	}

	/**
	 * @return the luogoNascita
	 */
	public String getLuogoNascita() {
		return birthplace;
	}

	/**
	 * @param luogoNascita
	 *            the luogoNascita to set
	 */
	public void setLuogoNascita(String luogoNascita) {
		this.birthplace = luogoNascita;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return numberphone;
	}

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.numberphone = telefono;
	}

	/**
	 * @return the dataNascita
	 */
	public Date getDataNascita() {
		return dataNascita;
	}

	/**
	 * @param dataNascita
	 *            the dataNascita to set
	 */
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	
	/**
	 * @return the attivita
	 */
	public Set<Attivita> getAttivita() {
		return attivita;
	}

	/**
	 * @param attivita the attivita to set
	 */
	public void setAttivita(Set<Attivita> attivita) {
		this.attivita = attivita;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Allievo [nome=" + name + ", cognome=" + surname + ", luogoNascita=" + birthplace + ", dataNascita="
				+ dataNascita + "]";
	}

}
