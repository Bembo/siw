package it.uniromatre.siw.siw2018.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ATTIVITA")
public class Attivita {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NAME", length = 1024)
	private String name;

	@Column(name = "DATA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "centro_id", nullable = false)
    private Centro centro;
    
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            })
    @JoinTable(name = "ALLIEVIATTIVITA",
            joinColumns = { @JoinColumn(name = "ATTIVITAID") },
            inverseJoinColumns = { @JoinColumn(name = "ALLIEVOID") })
    private Set<Allievo> allievi = new HashSet<>();
    
    public Attivita() {}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return name;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.name = nome;
	}

	/**
	 * @return the data
	 */
	public Date getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Attivita [id=" + id + ", nome=" + name + ", data=" + data + "]";
	}

	/**
	 * @return the centro
	 */
	public Centro getCentro() {
		return centro;
	}

	/**
	 * @param centro the centro to set
	 */
	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	/**
	 * @return the allievi
	 */
	public Set<Allievo> getAllievi() {
		return allievi;
	}

	/**
	 * @param allievi the allievi to set
	 */
	public void setAllievi(Set<Allievo> allievi) {
		this.allievi = allievi;
	}
	
	public void addAllievo(Allievo allievo) {
		if(null == this.allievi)
			this.allievi = new HashSet<>();
		
		this.allievi.add(allievo);
	}

}
