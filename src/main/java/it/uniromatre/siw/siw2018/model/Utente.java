package it.uniromatre.siw.siw2018.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UTENTE")
public class Utente {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "CENTRO")
	private Long idCentro;

	@Column(name = "TIPO")
	private Long tipologiaUtente; // 1 Azienda 2 Centro 3 allievo

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the idCentro
	 */
	public Long getIdCentro() {
		return idCentro;
	}

	/**
	 * @param idCentro
	 *            the idCentro to set
	 */
	public void setIdCentro(Long idCentro) {
		this.idCentro = idCentro;
	}

	/**
	 * @return the tipologiaUtente
	 */
	public Long getTipologiaUtente() {
		return tipologiaUtente;
	}

	/**
	 * @param tipologiaUtente
	 *            the tipologiaUtente to set
	 */
	public void setTipologiaUtente(Long tipologiaUtente) {
		this.tipologiaUtente = tipologiaUtente;
	}

}
