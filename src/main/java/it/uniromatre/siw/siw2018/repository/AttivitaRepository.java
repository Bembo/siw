package it.uniromatre.siw.siw2018.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import it.uniromatre.siw.siw2018.model.Attivita;
import it.uniromatre.siw.siw2018.model.Centro;

@Repository
public interface AttivitaRepository extends JpaRepository<Attivita, Long>{

	public List<Attivita> findByName(String name);
	
	public void deleteByName(String name);
	
	public List<Attivita> findByCentro(Centro centro);
	

}
