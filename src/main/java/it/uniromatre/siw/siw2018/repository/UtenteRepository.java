package it.uniromatre.siw.siw2018.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.uniromatre.siw.siw2018.model.Utente;

@Repository
public interface UtenteRepository extends JpaRepository<Utente, Long>{

	public List<Utente> findByUsernameAndPassword(String username, String password);
}
