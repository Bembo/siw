package it.uniromatre.siw.siw2018.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.uniromatre.siw.siw2018.model.Centro;

@Repository
public interface CentroRepository extends JpaRepository<Centro, Long> {

	public Centro findByNome(String nome);
	
}
