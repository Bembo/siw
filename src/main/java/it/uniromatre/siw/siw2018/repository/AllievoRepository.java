package it.uniromatre.siw.siw2018.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.uniromatre.siw.siw2018.dto.AllievoDto;
import it.uniromatre.siw.siw2018.model.Allievo;

@Repository
public interface AllievoRepository extends JpaRepository<Allievo, Long>{  

	public Allievo findByEmail(String email);

	public List<AllievoDto> findByNameAndSurnameAndEmail(String name, String surname, String email);
	
	public void deleteByEmail(String email);
}
