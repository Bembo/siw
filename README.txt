
L'interfaccia web � stata creata implementando una SPA (Single Page Application)
Il database usato � stato PostGres
Per far funzionare correttamente l'applicazione � necessario popolare la tabella "Utente" del DB,
La tabella Utente � formata da [ Id,centro,password,tipo,username ] 
dove:
	se Tipo Utente � uguale a 1 esso � l'azienda
	se Tipo Utente � uguale a 2 esso � il centro
	se Tipo Utente � uguale a 3 esso � un allievo qualsiasi


CASI D'USO : 

1) LOGIN : un utente vuole effettuare il login al sistema

2) AGGIUNGERE CENTRO : un nuovo centro vuole iscriversi al servizio offerto.Richiede dunque al direttore di inserire il centro nel sistema.

3) AGGIUNGERE ATTIVITA : il responsabile di un centro intende inserire una nuova attivit� per il suo centro.

4) MOSTRA ATTIVITA PER UTENTE : il sistema mostra le attivita dato un allievo "a"



Problemi : 

Manca l'interfaccia del front end per quanto riguarda la creazione di un nuovo allievo e l'associazione allievo-attivita anche se i servizi lato back-end sono presenti
(i servizi back-end implementano pi� casi d'uso descritti sopra)






